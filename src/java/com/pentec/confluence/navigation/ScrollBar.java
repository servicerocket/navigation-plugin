/*
 * Copyright (c) 2005, Ruben Miranda All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 *     modification, are permitted provided that the following conditions
 *     are met: 
 *   - Redistributions of source code must retain the above
 *     copyright notice, this list of conditions and the following
 *       disclaimer. 
 *   - Redistributions in binary form must reproduce the
 *       above copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided with
 *       the distribution. 
 *   - Neither the name of the Pentagon
 *       Technologies nor the names of its contributors may be used to
 *       endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 *     IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *     LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *     FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *     COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *     INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *     BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *     CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *     LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 * 
 * v1.0 created on: 01-Feb-2005 - Ruben Miranda
 * v1.1 updated on: 09-Feb-2005 - Ruben Miranda
 *      - addresses issue with bad links
 * v1.3 updated on: 23-Apr-2005 - David Peterson
 *      - Added 'up' link
 *      - Switched to using icons
 *      - Now works on parentless pages
 * v1.4 updated on: 24-Apr-2005 - David Peterson
 *      - Fixed bug with previewing a new, unsaved page.
 * v1.5 updated on: 25-Apr-2005 - David Peterson
 *      - Fixed rendering issue in Confluence 1.4+
 *      - Now sorts parentless pages.
 * v1.6 updated on: 14-May-2005 - David Peterson
 *      - Upgraded to the V2 Renderer. Is now Confluence 1.4+ exclusive.
 *      - Now only requires a single {scrollbar} if more than one is present on a page.
 */
package com.pentec.confluence.navigation;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.webwork.ServletActionContext;

/**
 * @author rmiranda, david@randombits.org
 */
public class ScrollBar extends BaseMacro
{
    private static final String DECORATOR_PARAM = "decorator";

    private static final String DEFAULT_CLASS_NAME = "Scrollbar";

    private static final String SCROLLBAR_TABLE = "ScrollbarTable";

    private static final String SCROLLBAR_PREV_ICON = "ScrollbarPrevIcon";

    private static final String SCROLLBAR_PREV_NAME = "ScrollbarPrevName";

    private static final String SCROLLBAR_PARENT = "ScrollbarParent";

    private static final String SCROLLBAR_NEXT_NAME = "ScrollbarNextName";

    private static final String SCROLLBAR_NEXT_ICON = "ScrollbarNextIcon";

    private static final String CLASS_PARAM = "class";

    private static String FALSE = "false";

    PageManager pageManager;

    private BootstrapManager bootstrapManager;

    SpaceManager spaceManager;

    public String execute(Map params, String body, RenderContext renderContext)
        throws MacroException
    {
        // String height = macroParameters.get("height");

        // Check if we are being decorated in a non-standard way.
        HttpServletRequest req = ServletActionContext.getRequest();
        if (req == null || req.getParameter(DECORATOR_PARAM) != null)
            return "";

        StringBuffer output = new StringBuffer();

        // Check if the scrollbar should allow going 'up' the tree.
        boolean up = !FALSE.equalsIgnoreCase((String) params.get("up"));

        // Check if icons should be used.
        boolean icons = !FALSE.equalsIgnoreCase((String) params.get("icons"));

        PageContext context = (PageContext) renderContext;

        String spaceKey = context.getSpaceKey();
        Page child, previousPage = null, nextPage = null;
        Page currentPage = pageManager
            .getPage(spaceKey, context.getPageTitle());

        if (currentPage == null) // the page hasn't been saved yet.
            return "";

        Page parentPage = currentPage.getParent();

        List children;
        if (parentPage == null)
            children = getOrphanedPages(spaceKey);
        else
            children = parentPage.getSortedChildren();

        if (children.size() > 0)
        {
            for (ListIterator listIterator = children.listIterator(); listIterator
                .hasNext();)
            {
                child = (Page) listIterator.next();
                if (child.equals(currentPage))
                {
                    if (listIterator.hasPrevious())
                    {
                        listIterator.previous();
                        if (listIterator.hasPrevious())
                            previousPage = (Page) listIterator.previous();
                        listIterator.next();
                    }

                    // back to original
                    if (listIterator.hasNext())
                    {
                        if (previousPage != null)
                            listIterator.next();
                        if (listIterator.hasNext())
                            nextPage = (Page) listIterator.next();
                    }
                    break;
                }
            }
        }

        String ctxRoot = bootstrapManager.getWebAppContextPath();
        String width = (parentPage == null || !up) ? "50%" : "33%";

        String className = (String) params.get(CLASS_PARAM);
        if (className == null)
            className = DEFAULT_CLASS_NAME;

        appendStyle(output);

        // GeneralUtil.urlEncode
        // output.append("<table style=\"border: none; padding: 3px;\"
        // border=\"0\" width=\"100%\" cellpadding=\"3\" cellspacing=\"0\"
        // bgcolor=\"#f0f0f0\"><tr>");
        output.append("<div").append(" class=\"").append(className).append("\">");
        output.append("<table class='").append(SCROLLBAR_TABLE).append("'><tr>");

        if (icons && previousPage != null)
        {
            // output.append("<td align=\"center\" width='16' style='text-align:
            // center; border: none'>");
            output.append("<td class='").append(SCROLLBAR_PREV_ICON).append(
                "'>");
            appendPageLink(output, ctxRoot, previousPage, makeIcon(ctxRoot,
                "back_16.gif"));
            output.append("</td>");
        }
        output.append("<td width='").append(width).append("' class='").append(
            SCROLLBAR_PREV_NAME).append("'>");
        if (previousPage != null)
            appendPageLink(output, ctxRoot, previousPage);
        output.append("&nbsp;</td>");

        if (up && parentPage != null)
        {
            output.append("<td width='").append(width).append("' class='")
                .append(SCROLLBAR_PARENT).append("'>");

            if (icons)
            {
                output.append("<sup>");
                appendPageLink(output, ctxRoot, parentPage, makeIcon(ctxRoot,
                    "up_16.gif", 8));
                output.append("</sup>");
            }
            appendPageLink(output, ctxRoot, parentPage);
            output.append("</td>");
        }

        output.append("<td width='").append(width).append("' class='").append(
            SCROLLBAR_NEXT_NAME).append("'>");
        output.append("&nbsp;");
        if (nextPage != null)
            appendPageLink(output, ctxRoot, nextPage);
        output.append("</td>");

        if (icons && nextPage != null)
        {
            output.append("<td class='").append(SCROLLBAR_NEXT_ICON).append(
                "'>");
            appendPageLink(output, ctxRoot, nextPage, makeIcon(ctxRoot,
                "forwd_16.gif"));
            output.append("</td>");
        }

        output.append("</tr></table></div>");

        return output.toString();
    }

    /**
     * Creates the style block for this TOC, returning the class name generated.
     *
     * @param out The string buffer to output into.
     */
    private void appendStyle(StringBuffer out)
    {
        String prefix = "table." + SCROLLBAR_TABLE + " ";

        out.append("<style type='text/css'>/*<![CDATA[*/\n");

        out.append(prefix).append(" {");
        out.append("border: none;");
        out.append("padding: 3px;");
        out.append("width: 100%;");
        out.append("padding: 3px;");
        out.append("margin: 0px;");
        out.append("background-color: #f0f0f0");
        out.append("}\n");

        out.append(prefix).append("td.").append(SCROLLBAR_PREV_ICON).append(
            " {");
        out.append("text-align: center;");
        out.append("width: 16px;");
        out.append("border: none;");
        out.append("}\n");

        out.append(prefix).append("td.").append(SCROLLBAR_PREV_NAME).append(
            " {");
        out.append("text-align: left;");
        out.append("border: none;");
        out.append("}\n");

        out.append(prefix).append("td.").append(SCROLLBAR_PARENT).append(" {");
        out.append("text-align: center;");
        out.append("border: none;");
        out.append("}\n");

        out.append(prefix).append("td.").append(SCROLLBAR_NEXT_NAME).append(
            " {");
        out.append("text-align: right;");
        out.append("border: none;");
        out.append("}\n");

        out.append(prefix).append("td.").append(SCROLLBAR_NEXT_ICON).append(
            " {");
        out.append("text-align: center;");
        out.append("width: 16px;");
        out.append("border: none;");
        out.append("}\n");

        out.append("\n/*]]>*/</style>");
    }

    private void appendPageLink(StringBuffer output, String ctxRoot, Page page)
    {
        appendPageLink(output, ctxRoot, page, page.getTitle());
    }

    private void appendPageLink(StringBuffer output, String ctxRoot, Page page,
                                String title)
    {
        if (title == null)
            title = page.getTitle();

        output.append("<a href=\"").append(ctxRoot).append(page.getUrlPath()).append("\">");
        output.append(title);
        output.append("</a>");
    }

    private String makeIcon(String ctxRoot, String image)
    {
        return makeIcon(ctxRoot, image, 16);
    }

    private String makeIcon(String ctxRoot, String image, int size)
    {
        return "<img border='0' align='middle' src='" + ctxRoot
            + "/images/icons/" + image + "' width='" + size + "' height='"
            + size + "'>";
    }

    private List getOrphanedPages(String spaceKey)
    {
        List orphanedPages = pageManager.getOrphanedPages(spaceKey);

        // Sort the pages.
        Collections.sort(orphanedPages, new Comparator()
        {
            public int compare(Object o1, Object o2)
            {
                Page p1 = (Page) o1;
                Page p2 = (Page) o2;
                return p1.getTitle().compareTo(p2.getTitle());
            }
        });

        return orphanedPages;
    }

    /**
     * @return "scrollbar"
     */
    public String getName()
    {
        return "scrollbar";
    }

    /**
     * @param spaceManager The SpaceManager instance.
     */
    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    /**
     * @param pageManager The PageManager instance.
     */
    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    /**
     * @param bootstrapManager The BootstrapManager instance.
     */
    public void setBootstrapManager(BootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

}